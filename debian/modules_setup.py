import sys
from setuptools import setup, find_packages

name = "modules"
# packages = find_packages(['annlib_adaptbx', 'ccp4io_adaptbx', 'dxtbx', 'txtbx'])
packages = find_packages('.', exclude=['cctbx_project', 'cbflib'])
package_dir={name: '.'}

requirements = ["six"]

setup_requirements = []
needs_pytest = {"pytest", "test", "ptr"}.intersection(sys.argv)
if needs_pytest:
    setup_requirements.append("pytest-runner")

test_requirements = ["mock", "pytest"]

setup(
    author="Diamond Light Source",
    author_email="scientificsoftware@diamond.ac.uk",
    classifiers=[
        "Development Status :: 1 - Planning",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: BSD License",
        "Natural Language :: English",
        "Operating System :: OS Independent",
        "Programming Language :: Python :: 2",
        "Programming Language :: Python :: 2.7",
        "Programming Language :: Python :: Implementation :: CPython",
    ],
    description="Diffraction Integration for Advanced Light Sources",
    install_requires=requirements,
    license="BSD license",
    include_package_data=True,
    keywords=name,
    name=name,
    packages=packages,
    package_dir=package_dir,
    setup_requires=setup_requirements,
    test_suite="tests",
    tests_require=test_requirements,
    url="https://github.com/cctbx/cctbx",
    version="0.0.1",
    zip_safe=False,
)
